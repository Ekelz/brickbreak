import javax.swing.JButton;
import javax.swing.JFrame;

public class board {
	
	public static void main(String[] args) {
		JFrame frame = new JFrame("Brick Break !");
		
		/*
		 Mise en place de la fen�tre du menu principal.
		 */
		JFrame beginFrame = new JFrame("Brick Break Menu");
		JButton start = new JButton("Nouvelle partie");
		
		GamePanel panel = new GamePanel(frame,beginFrame);
		
		/*
		 D�finition d'un Listener pour afficher la fen�tre de jeu quand on clique sur 'Nouvelle partie'.
		 */
		start.addActionListener(listener -> {
			beginFrame.setVisible(false);
			frame.setVisible(true);
		});
		
		/*
		 Dimensions et configuration de base des deux fen�tres (jeu : 'frame' et menu : 'beginframe')
		 */
		
		frame.getContentPane().add(panel);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.setVisible(false);
		frame.setSize(490, 600);
		
		frame.setResizable(false);
		
		beginFrame.getContentPane().add(start);
		
		beginFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		beginFrame.setVisible(true);
		beginFrame.setSize(490, 600);
		
		beginFrame.setResizable(false);
	}

}
