import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class GamePanel extends JPanel implements KeyListener {

	/*
	 Ici on a tout le fonctionnement du jeu.
	 On a l'affichage de la barre et des briques, la gestion de la vitesse d'affichage.
	 
	 On dispose �galement des m�thodes pour r�initialiser l'affichage en cas de fin de partie
	 ainsi que les Listeners qui g�rent les d�placements de la barre.
	 */
	
	private static final long serialVersionUID = 1L;
	ArrayList<Brick> blocks;
	Brick ball;
	Brick sweep;
	
	JFrame mainFrame,startScreen;
	
	Thread thread;
	
	void reset() {
		blocks = new ArrayList<Brick>();
		ball = new Brick(237, 435, 35, 25, "src\\ball.png");
		sweep = new Brick(175, 480, 150, 25, "src\\paddle.png");
		
		/*
		 On ajoute les briques par rang�e grace � un 'for', on passe les dimensions des briques et l'image
		 utilis�e dans les ressources pour l'afficher.
		 */
		for (int i = 0; i < 8; i++)
			blocks.add(new Brick((i*60+2),0,60,25,"src\\brick_blue.png"));
		for (int i = 0; i < 8; i++)
			blocks.add(new Brick((i*60+2),25,60,25,"src\\brick_red.png"));
		for (int i = 0; i < 8; i++)
			blocks.add(new Brick((i*60+2),50,60,25,"src\\brick_green.png"));
		for (int i = 0; i < 8; i++)
			blocks.add(new Brick((i*60+2),75,60,25,"src\\brick_yellow.png"));

		
		addKeyListener(this);
		setFocusable(true);
	}

	
	GamePanel(JFrame frame, JFrame startScreen) {
		
		this.mainFrame = frame;
		this.startScreen = startScreen;
	

		reset();
		
		/*
		 Ici on impl�mente un thread qui va g�rer le d�roulement du jeu.
		 La fonction update est appel�e continuellement pour faire bouger la balle et
		 adopter le comportement ad�quat pour la barre et les blocs.
		 
		 Entre deux updates, le thread va attendre un certain nombre de millisecondes avec la
		 fonction 'Thread.sleep'. Cela permet d'adapter la vitesse d'ex�cution du jeu (si on veut un jeu lent,
		 on augmente la valeur dans le sleep).
		 */
		thread = new Thread(() -> {
			while (true) {
				update();
				try {
					Thread.sleep(15);
				} catch (InterruptedException err) {
					err.printStackTrace();
				}
			}
		});
		thread.start();
		
	}

	/*
	 Ici on impl�mente la m�thode d'affichage graphique des briques, de la balle et de la barre.
	 */
	public void paintComponent(Graphics g) {
		 g.setColor(getBackground()); 
		 g.fillRect(0, 0, this.getWidth(), this.getHeight()); 
		blocks.forEach(block -> {
			block.draw(g, this);
		});
		ball.draw(g, this);
		sweep.draw(g, this);
	}

	/*
	 Cette m�thode va transmettre � la balle ses nouvelles coordonn�es afin d'obtenir un mouvement constant.
	 Cette m�thode applique l'�tat 'touched' aux briques touch�es par la balle.
	 
	 Elle g�re �galement la situation o� la balle passerait en-dessous de la fen�tre pour revenir au menu.
	 On utilise reset dans ce cas pour r�initialiser l'�tat de la f�n�tre au cas o� une nouvelle partie 
	 soit lanc�e.
	 */
	public void update() {

		 
		ball.x += ball.movX;
		
		if(ball.x > (getWidth() - 25) || ball.x < 0)
			ball.movX *= -1;
		
		if(ball.y < 0 || ball.intersects(sweep))
			ball.movY *= -1;
		
		ball.y += ball.movY;
		
		if(ball.y > getHeight()) {
			thread = null;
			reset();
			mainFrame.setVisible(false);
			startScreen.setVisible(true);
			
		}
		
		blocks.forEach(block -> {
			if(ball.intersects(block) && !block.touched) {
				ball.movY *=-1;
				block.touched = true;
				
			}
		});
		repaint();

	}

	
	/*
	 Ici sont impl�ment�es les Listener pour le mouvement de la barre.
	 Un eventListener 'KeyDown' est pr�sent pour permettre de d�placer la barre en maintenant les
	 touches fl�ch�es.
	 */
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
			

		if (e.getKeyCode() == KeyEvent.VK_RIGHT && sweep.x < (getWidth() - sweep.width)) {
			sweep.x += 1;
		}

		if (e.getKeyCode() == KeyEvent.VK_LEFT && sweep.x > 0) {
			sweep.x -= 1;
		}

	}
	
	public void keyDown(KeyEvent e, int key) {
		
		if (e.getKeyCode() == KeyEvent.VK_RIGHT && sweep.x < (getWidth() - sweep.width)) {
			sweep.x += 1;
		}

		if (e.getKeyCode() == KeyEvent.VK_LEFT && sweep.x > 0) {
			sweep.x -= 1;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
