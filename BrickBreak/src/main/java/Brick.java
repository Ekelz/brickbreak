import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Brick extends Rectangle {

	/*
	 Ici nous allons cr�er les attributs de la classe qui sera appel�e par les briques et par la barre pour les
	 afficher.
	 
	 On a �galement la m�thode 'draw' qui v�rifie si la brique a �t� touch�e et arr�te
	 de l'afficher si c'est le cas. (attribut bool�en 'touched')
	 */
	private static final long serialVersionUID = 1L;
	Image pic;
	boolean touched;
	
	int movX,movY;

	Brick(int x, int y, int w, int h, String s) {
		this.x = x;
		this.y = y;
		File img = new File(s);
		
		movX = 3;
		movY = 3;

		this.width = w;
		this.height = h;
		
		try {
			pic = ImageIO.read(img);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void draw(Graphics g, Component c) {
		if (!touched)
			g.drawImage(pic, x, y, width, height, c);
	}

}
